<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Class ArticleController
 * @package AppBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * @Route("/", name="article_index")
     */
    public function indexAction()
    {
        $article = $this->getDoctrine()->getRepository(Article::class);
        $article = $article->findAll();
        return $this->render('article/index.html.twig', [
            'article' => $article]);
    }
}